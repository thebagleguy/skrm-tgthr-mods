If installing Vivid Weathers Manually :

REQUIRED : 
- Merge the content of the "Data" folder of this archive with your orginal Data folder.

OPTIONALS :
- If you want to use the new galaxy and stars textures, intall of the "Galaxyandstars" folder.
- Install relevant compatibility patches from the "Compatibiltiy patches" folder.

RECOMMENDED :
- Use a mod manager next time.