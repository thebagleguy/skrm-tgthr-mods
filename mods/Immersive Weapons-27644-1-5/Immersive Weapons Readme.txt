Immersive Weapons by Ironman5000 & Hothtrooper44 & Eckss
================================================


Description
===========

Adds many lore friendly weapons to the game that will be obtainable through crafting, vendors, loot, and equipped by NPCs.
It works by merging some of the best lore friendly weapon mods on the nexus into one plugin file, while packaging the data files into one tidy bsa, then adding them to many levelled lists/containers/NPCs throughout the game so that they appear just like vanilla weapons and greatly enhances weapon variety.
All weapons used have had their stats tweaked from the originals to keep the game balanced - there will be no overpowered weapons at all and stronger ones will appear as you raise your level.
Spawns have been set up by design - evil looking weapons will appear on enemies, fancy weapons in towns and estates.  There are even some familiar uniques to be found appropriately throughout the game, check the credits for a better clue.  No edits to cells or worldspaces have been made to avoid conflicts.
Some weapons have been retextured for this project to improve their aesthetic and make them look that they belong with vanilla weapons.

Immersive Weapons contains many unusual weapons, but they all fit into the basic weapon categories, and all weapon category names are taken from real historical weapons.

Weapons categories:

Battleaxe - Double Axe; Halberd; Scythe; Spear

Double Axes are heavier, hit harder and cause more Damage.
Halberds have longer reach and hit harder than a Battleaxe, but not as hard as a Double Axe.
Scythes are lighter and faster, but don�t do as much Damage
Spears are much lighter, much faster and have the longest reach, but don�t hit as hard and do the least damage.

Dagger � Tanto

Tantos are heavier and slower, but do more damage.

Greatsword � Changdao; Claymore; Dadao; Daito; Longsword; Nodachi

Changdaos are shorter, lighter, faster and do less damage.
Claymores are also lighter, faster and do less damage, and are shorter than a Changdao.
Dadaos are Heavier, harder hitting and more damaging, but are also shorter than a Changdao.
Daitos are lighter, faster, less hard hitting and do less damage and are also shorter than a Changdao.
Longswords are shorter still, lighter still, less hard hitting and even less damaging, but much faster.
Nodachis are shorter, lighter, and faster, less hard hitting and do less damage.

Mace � Club; War Pick

War Picks and Clubs are both lighter and less hard hitting, but faster.
War Picks hit harder than Clubs.

Sword � Cutlass; Katana; Ninjato; Saber; Scimitar; Wakizashi

Cutlasses are shorter.
Katanas are longer, but less hard hitting.
Ninjatos are lighter, even less hard hitting and less damaging, but faster.
Sabers are lighter and less hard hitting, but faster.
Scimitars are shorter and less hard hitting, but faster.
Wakizashi are even lighter, shorter still, less damaging and hit not at all hard.

Warhammer; Battle Staff; Quarterstaff

Both types of Staff are lighter, don�t hit as hard and do much less damage.
The Battle Staff is the harder hitting and more damaging Staff.
The Quarterstaff is the lighter and faster Staff.


Changelog V1.5
==============

- Converted Immersive Weapons from SPIKE, to SPIKE � Virtual Edition � No more dependence on SPIKE.esm
- Removed Weapons with unfixable bugs: Blue Divide Bow; Draugr Club; Forebear Bow; Hammerfell Bow; Hammerfell Hunters Bow; Hammerfell M'kai Bow; Prince A'Tor Bow
- Added 7 new bows
- Changed to Crafting-Only: Pokeblade; Splitter; Bipolar blade
- Re-incorporated the Spearblade as Crafting-Only, by popular request
- Reorganized and re-balanced weapon distribution system
- Overhauled crafting/tempering/breakdown recipes & added missing ones 
- Converted all Dragonbone weapons to Dragonsteel to prevent confusion with other Dragonbone weapons
- Incorporated a fix for the vanilla Skyrim Silver Weapon bug 
- Limited Imperial soldiers to appropriate 1 Handed swords 
- Limited Stormcloaks to appropriate Rebel weapons
- Removed all staff weapons from guards and soldiers
- Thresher Maul redefined & distributed as a weaponized agricultural implement (metal covered wood)
- Implemented a naming convention and standards for all non-unique weapons based on historical examples
- Implemented Nordic category on forging menu (incorporating Ancient Nord & Draugr equipment)
- Implemented An-Xileel War Bow for Argonians, Nordic Scrimshaw Bow for Nords and Valenwood War Bow for Bosmer to go alongside the Ornate Khajiit Bow, Orsimer Chieftain Bow, Colovian Composite Bows and Elven Hawk Bows as Racial themed bows
- Redistributed weapons among forging categories
- Added option to craft replicas of Daedric Artifact weapons (& other limited unique vanilla weapons) once the originals have been obtained
- Azura�s Moon now requires possession of Azura�s Star before it can be crafted
- Wabba Jackhammer & Wabba Jaxe now require possession of Wabbajack before either can be crafted
- Increased incidence of dual wielding enemies
- Fixed Crow Sword Scabbard NPC 3D Model
- Fixed various issues in weapon records (sounds, criticals, Impact Data Sets, &tc)
- Fixed various minor bugs/inconsistencies

Changelog V1.4
==============

- Got rid of the few dirty edits that were present in 1.3 
- All staff weapons are used like warhammers, benefitting from those perks
- The battle staff and quarterstaff damage has been lowered to favour balance
- Staff recipes have been balanced, and the Steel Quarterstaff one has been fixed
- All exotic weaponry (and staffs, definately this time!) have been removed from all guards and soldiers, by popular request
- Exotic weapons have been taken from bandits
- Umbra is now renamed 'Umbra Replica' to satisfy lore confusion
- Fixed all weapon categories - for example mace looking weapons are all classed as maces now
- Fixed keywords on the few weapons that were causing them to be recognised incorrectly

Changelog V1.3
==============

- Added Dragonbone Battleaxe, Scythe, and Magic Staffs
- Added the Executioner Hook
- Added Several new bows from Better Bows
- New levelled lists made for guards so they will no longer carry staffs
- Removed the Afterslash by request
- Removed the Spearblade by request
- Removed the Hammerfell Iron Sword by request
- Removed the text from silver weapons to enable to see enchant effects
- Changed the silver weapon required smithing perk to elven
- Fixed a few grammar errors
- Fixed perk related issues for some weapons

Changelog V1.2
==============

- Fixed some recipes that needed to be appropriate
- Fixed the Exotic Arrow projectile
- Added Quartertaffs for each weapon category, coutesy of JZBai, Fully integrated.
- Removed the slipcleave by request

Changelog V1.1
==============

- Fixed the flipped normals on some weapons which caused bad lighting
- Fixed badly compressed normal map textures which gave a blotchy effect on some weapons
- Removed the Hammerfell Spears due to not working correctly
- Added the Nordic Spears
- Retextured a couple of weapon to improve them
- Replaced the glass scimitar mesh supposedly causing CTDs, with the 'fix' from the WOT3E page.
- Removed the Snake Axe by request

Changelog V1.0 (starting it here so you can see the list of features)
==============

- All weapons can be crafted at any forge and tempered on crafting wheels, you will need appropriate perks for crafting weapons of increasing strength as you level
- Weapons will spawn in merchant inventory and change/spawn every few days, getting better as you level
- All NPCs will have a chance to equip the new weapons, and will change as you level.  You will find weapons on some people that you won't on others, depending on their inclination
- You will find them as loot on chests, better ones will appear in 'boss' chests
- All weapons are enchantable
- All weapons can be broken down into base components using the smelter
- Silver Weapons are especially effective against undead
- Retextured several weapons (since the beta release) for more fitting aesthetic.


Installation
============

Place these into your data folder

- Immersive Weapons.esp
- Immersive Weapons.bsa

To uninstall simply remove them.


Compatibility
=============

If you have any other mods that allow weapons to spawn throughout the game in some fashion, the one that loads last will be the one that allows world spawns but all will remain craftable.
Check the credits, if you have any of these mods active you should remove them from the load order to avoid seeing duplicates but will not cause any bugs.


Credits for the original work
=============================

Disclaimer: All weapon mods listed below are either part of weapon resource mods, or permissions have been personally gained by the authors themselves.
Big thanks go to all the authors so be be sure to endorse their mods as well as this, it's only fair.

List of authors and their included mods:

- 747823 for Weapons of the Third Era - http://skyrim.nexusmods.com/mods/3871
- InsanitySorrow for Dragonbane - http://skyrim.nexusmods.com/mods/19180, Ice Blade of the Monarch - http://skyrim.nexusmods.com/mods/18597, Dragon Katana - http://skyrim.nexusmods.com/mods/15955, Chrysamere - http://skyrim.nexusmods.com/mods/15545, Goldbrand - http://skyrim.nexusmods.com/mods/15431, Umbra Sword - http://skyrim.nexusmods.com/mods/15422,
- Urwy for URW Useless Resources - http://skyrim.nexusmods.com/mods/12087
- Dogtown1 for the weapons from Skyrim Monster Mod - http://skyrim.nexusmods.com/mods/9694
- PrivateEye for BiPolar Blade - http://skyrim.nexusmods.com/mods/15579, Sixth House Bell Hammer - http://skyrim.nexusmods.com/mods/17534, The Fork of Horripilation - http://skyrim.nexusmods.com/mods/14923, Mace of Aevar Stone-Singer - http://skyrim.nexusmods.com/mods/14429
- AlexScorpion for the Exotic Bow - http://skyrim.nexusmods.com/mods/13911, Blades of Sithis - http://skyrim.nexusmods.com/mods/2715, Dragon Ninjatos - http://skyrim.nexusmods.com/mods/2480
- Cokla for the scimitar from Mongol Armor Standalone - http://skyrim.nexusmods.com/mods/8561, YesThor Swords Standalone - http://skyrim.nexusmods.com/mods/6487, Elven Great Blade - http://skyrim.nexusmods.com/mods/5074, Barbarian Axe - http://skyrim.nexusmods.com/mods/4723, Double Axes Of Skyrim - http://skyrim.nexusmods.com/mods/4293, Dwarven Slayer - http://skyrim.nexusmods.com/mods/4018
- gechbal for The Sword Of The One Eyed Crow - http://skyrim.nexusmods.com/mods/3072, and Orgnums Dagger - http://skyrim.nexusmods.com/mods/14975
- ImsumDave for Black Sting - http://skyrim.nexusmods.com/mods/13675
- Sushiyant for Hot Blood - http://skyrim.nexusmods.com/mods/5175, Ariyan Weapon Pack - http://skyrim.nexusmods.com/mods/13218
- wulfharth for Wulfharths Dragonbone Weapons - http://skyrim.nexusmods.com/mods/10171
- siberok92 for Chakram Weapon - http://skyrim.nexusmods.com/mods/19466, Thresher Maul - http://skyrim.nexusmods.com/mods/18843, Phoenix Katana - http://skyrim.nexusmods.com/mods/16935
- RonnieMagnum for Ordinary weapons for Skyrim - Redguard blades - http://skyrim.nexusmods.com/mods/25442
- JZBai for Quarterstaff - http://skyrim.nexusmods.com/mods/25431
- atomec fpr Orcish Sabre - http://skyrim.nexusmods.com/mods/25361, Svartbough - http://skyrim.nexusmods.com/mods/23244, Ceymallari - http://skyrim.nexusmods.com/mods/21617
- TESFAN123 for Aries and Tombstone Norse Great Swords - http://skyrim.nexusmods.com/mods/24641
- hifoo for Cutlass - http://skyrim.nexusmods.com/mods/23814
- backstept for Regent Armoury - http://skyrim.nexusmods.com/mods/17250
- Issae for Nordic Spears - http://skyrim.nexusmods.com/mods/15294
- howiego08 for Better Bows - http://skyrim.nexusmods.com/mods/13317
- luddemann for Executioners Hook - http://skyrim.nexusmods.com/mods/28938


Permissions
===========

DO NOT UPLOAD THIS FILE TO ANY OTHER SITE UNDER ANY CIRCUMSTANCES.
Do not request any permissions from me to use any assets from this mod for your own projects, I do not have the grounds to allow redistribution.
If anyone wants to make a translated version, be sure to only make an esp file for your page that will require downloading this mod.  Ask first though.


Feedback
========

This mod has been thoroughly beta tested so there should be no bugs, however if you spot anything or have any questions please let use know in the comments.